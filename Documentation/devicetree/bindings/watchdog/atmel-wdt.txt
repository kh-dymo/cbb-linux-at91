* Atmel Watchdog Timers

** at91sam9-wdt

Required properties:
- compatible: must be "atmel,at91sam9260-wdt".
- reg: physical base address of the controller and length of memory mapped
  region.

Optional properties:
- timeout-sec: contains the watchdog timeout in seconds.
- early-timeout-sec: If present, specifies a timeout value in seconds
  that the driver keeps on ticking the watchdog HW on behalf of user
  space. Once this timeout expires watchdog is left to expire in
  timeout-sec seconds. If this propery is set to zero, watchdog is
  started (or left running) so that a reset occurs in timeout-sec
  since the watchdog was started.
- interrupts : Should contain WDT interrupt.
- atmel,max-heartbeat-sec : Should contain the maximum heartbeat value in
	seconds. This value should be less or equal to 16. It is used to
	compute the WDV field.
- atmel,min-heartbeat-sec : Should contain the minimum heartbeat value in
	seconds. This value must be smaller than the max-heartbeat-sec value.
	It is used to compute the WDD field.
- atmel,watchdog-type : Should be "hardware" or "software". Hardware watchdog
	use the at91 watchdog reset. Software watchdog use the watchdog
	interrupt to trigger a software reset.
- atmel,reset-type : Should be "proc" or "all".
	"all" : assert peripherals and processor reset signals
	"proc" : assert the processor reset signal
	This is valid only when using "hardware" watchdog.
- atmel,disable : Should be present if you want to disable the watchdog.
- atmel,idle-halt : Should be present if you want to stop the watchdog when
	entering idle state.
- atmel,dbg-halt : Should be present if you want to stop the watchdog when
	entering debug state.

Example:
	watchdog@fffffd40 {
		compatible = "atmel,at91sam9260-wdt";
		reg = <0xfffffd40 0x10>;
		interrupts = <1 IRQ_TYPE_LEVEL_HIGH 7>;
		timeout-sec = <15>;
		early-timeout-sec = <120>;
		atmel,watchdog-type = "hardware";
		atmel,reset-type = "all";
		atmel,dbg-halt;
		atmel,idle-halt;
		atmel,max-heartbeat-sec = <16>;
		atmel,min-heartbeat-sec = <0>;
		status = "okay";
	};
